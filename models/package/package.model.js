import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const PackageSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    name: {
        type: String,
        trim: true,
        required: true,
    },
    description:{
        type: String,
        trim: true,
        required: true,
    },
    start:{
        type:Number,
        required:true
    },
    end:{
        type:Number,
        required:true
    },
    discount:{
        type:Number,
        required:true
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

PackageSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
PackageSchema.plugin(autoIncrement.plugin, { model: 'package', startAt: 1 });

export default mongoose.model('package', PackageSchema);