import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const BillSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    user:{
        type:Number,
        ref:'user'
    },
    number: {
        type: Number,
        trim: true,
        required: true,
    },
    shop:{
        type:Number,
        ref:'shop'
    },
    branch:{
        type:Number,
        ref:'branch'
    },
    price:{
        type:Number,
        required:true
    },
    priceAfterDiscount:{
        type:Number,
        required:true
    },
    type:{
        type: String,
        enum: ['DONE', 'CURRENT','REFUSED'],
        required:true,
        default:'CURRENT'
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

BillSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
    }
});
autoIncrement.initialize(mongoose.connection);
BillSchema.plugin(autoIncrement.plugin, { model: 'bill', startAt: 1 });

export default mongoose.model('bill', BillSchema);