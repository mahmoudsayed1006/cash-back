import mongoose, { Schema } from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';


const BranchSchema = new Schema({

    _id: {
        type: Number,
        required: true
    },
    branchName: {
        type: String,
        required: true,
        trim: true
    },
    shop: {
        type: Number,
        ref: 'shop'
    },
    deleted:{
        type:Boolean,
        default:false
    }
});

BranchSchema.set('toJSON', {
    transform: function (doc, ret) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
        delete ret.deleted;
    }
});


BranchSchema.plugin(autoIncrement.plugin, { model: 'branch', startAt: 1 });

export default mongoose.model('branch', BranchSchema);
