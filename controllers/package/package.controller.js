import ApiResponse from "../../helpers/ApiResponse";
import Package from "../../models/package/package.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let packages = await Package.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const packagesCount = await Package.count(query);
            const pageCount = Math.ceil(packagesCount / limit);

            res.send(new ApiResponse(packages, page, pageCount, limit, packagesCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('name').not().isEmpty().withMessage('name is required')
                .custom(async (val, { req }) => {
                    let query = { name: val, deleted: false };
                    console.log(query);
                    if (isUpdate)
                        query._id = { $ne: req.params.packageId };
                    let packages = await Package.findOne(query).lean();
                    if (packages)
                        throw new Error('Package duplicated name');

                    return true;
                }),
            body('description').not().isEmpty().withMessage('description is required'),
            body('start').not().isEmpty().withMessage('start is required'),
            body('end').not().isEmpty().withMessage('end is required'),
            body('discount').not().isEmpty().withMessage('discount is required'),
        ];
        
        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            let createdPackage = await Package.create({ ...validatedBody});

            let reports = {
                "action":"Create Package",
            };
            let report = await Report.create({...reports, user: user });
            res.status(201).send(createdPackage);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { packageId } = req.params;
            await checkExist(packageId,Package, { deleted: false });
            let packages = await Package.findById(packageId);
            res.send(packages);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { packageId } = req.params;
            await checkExist(packageId, Package, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedPackage = await Package.findByIdAndUpdate(packageId, {
                ...validatedBody,
            }, { new: true });
            let reports = {
                "action":"Update Package",
            };
            let report = await Report.create({...reports, user: user });
            res.status(200).send(updatedPackage);
        }
        catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { packageId } = req.params;
            let packages = await checkExistThenGet(packageId, Package, { deleted: false });
            packages.deleted = true;
            await packages.save();
            let reports = {
                "action":"Delete Package",
            };
            let report = await Report.create({...reports, user: user });
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};