import ApiResponse from "../../helpers/ApiResponse";
import Shop from "../../models/shop/shop.model";
import Bill from "../../models/bills/bills.model";
import Package from "../../models/package/package.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet, isImgUrl } from "../../helpers/CheckMethods";
import { handleImg, checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import { sendNotifiAndPushNotifi } from "../../services/notification-service";
import Notif from "../../models/notif/notif.model";

const populateQuery = [
    { path: 'shop', model: 'shop'},
    { path: 'branch', model: 'branch' },
    { path: 'user', model: 'user' },

];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {type,shopId,branchId} = req.query;
            let query = { deleted: false };
            if(type) query.type = type;
            if(shopId) query.shop = shopId;
            if(branchId) query.branch =branchId;
            let bills = await Bill.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const billsCount = await Bill.count(query);
            const pageCount = Math.ceil(billsCount / limit);

            res.send(new ApiResponse(bills, page, pageCount, limit, billsCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('number').not().isEmpty().withMessage('number is required')
                .custom(async (val, { req }) => {
                    let query = { number: val, deleted: false };
                    console.log(query);
                    if (isUpdate)
                        query._id = { $ne: req.params.billId };
                    let bills = await Bill.findOne(query).lean();
                    if (bills)
                        throw new Error('Bill duplicated name');

                    return true;
                }),
            body('shop').not().isEmpty().withMessage('shop is required'),
            body('branch').not().isEmpty().withMessage('branch is required'),
            body('price').not().isEmpty().withMessage('price is required'),
        ];
        
        return validations;
    },

    async create(req, res, next) {

        try {
            const validatedBody = checkValidations(req);
          

            let shop = await checkExistThenGet(validatedBody.shop, Shop, { deleted: false });
            let user = await checkExistThenGet(req.user._id, User, { deleted: false });
            let userPackage = await checkExistThenGet(user.package, Package, { deleted: false });

            let discount = userPackage.discount;
            let newPrice = validatedBody.price - (validatedBody.price * discount)/100;
            user.totalPurchases = user.totalPurchases + newPrice;
            user.currentPurchases = user.currentPurchases + newPrice;
            user.amountAvailable = user.amountAvailable + (validatedBody.price - newPrice);
            user.save();
            let createdBill = await Bill.create({ ...validatedBody,user:req.user,priceAfterDiscount:newPrice});
            sendNotifiAndPushNotifi({
                targetUser: shop.user, 
                fromUser: req.user, 
                text: 'new notification',
                subject: createdBill.id,
                subjectType: 'new bill'
            });
            let notif = {
                "description":'new bill'
            }
            await Notif.create({...notif,resource:req.user,target:shop.user,subject:createdBill.id});
            let reports = {
                "action":"Create Bill",
            };
            let report = await Report.create({...reports, user: user,belong:'SHOP' });

            //let user = await checkExistThenGet(req.user._id,User);
            //let userPackage = await checkExistThenGet(user.package,Package);
            let query = { deleted: false };
            let packages = await Package.find(query).select('_id');
            var array = [];
            for(var i = 0; i < packages.length; i++){
                array.push(packages[i]._id)
            }
            console.log(array);
            let index = array.indexOf(user.package)
            console.log(packages[index + 1]);
            if(user.currentPurchases > userPackage.end){
                user.package = packages[index + 1];
            }
            await user.save();
            res.status(201).send(createdBill);
        } catch (err) {
            next(err);
        }
    },


    async findById(req, res, next) {
        try {
            let { billId } = req.params;
            await checkExist(billId,Bill, { deleted: false });
            let bills = await Bill.findById(billId).populate(populateQuery);
            res.send(bills);
        } catch (err) {
            next(err);
        }
    },
    async accept(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'SHOP')
                return next(new ApiError(403, ('shop.auth')));

            let { billId} = req.params;
            let acceptBill = await checkExistThenGet(billId,Bill);
            acceptBill.type = 'DONE';
            await acceptBill.save();
            let reports = {
                "action":"bill accept",
            };
            let report = await Report.create({...reports, user: user,belong:'SHOP' });
            sendNotifiAndPushNotifi({
                targetUser: acceptBill.user, 
                fromUser: req.user, 
                text: 'new notification',
                subject: acceptBill.id,
                subjectType: 'cash back accept your bill'
            });
            let notif = {
                "description":'cash back accept your bill'
            }
            await Notif.create({...notif,resource:req.user,target:acceptBill.user,subject:acceptBill.id});
            res.send('bill accept');
            
        } catch (error) {
            next(error);
        }
    },

    async refused(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'SHOP')
                return next(new ApiError(403, ('shop.auth')));

            let { billId} = req.params;
            let acceptBill = await checkExistThenGet(billId,Bill);
            acceptBill.type = 'REFUSED';
            await acceptBill.save();
            let reports = {
                "action":"bill refuced",
            };
            let report = await Report.create({...reports, user: user,belong:'SHOP' });
            sendNotifiAndPushNotifi({
                targetUser: acceptBill.user, 
                fromUser: req.user, 
                text: 'new notification',
                subject: acceptBill.id,
                subjectType: 'cash back refused your bill'
            });
            let notif = {
                "description":'cash back refused your bill'
            }
            await Notif.create({...notif,resource:req.user,target:acceptBill.user,subject:acceptBill.id});
            res.send('bill refused');
        } catch (error) {
            next(error);
        }
    },
    async delete(req, res, next) {
        try {
            let user = req.user;
            let { billId } = req.params;
            let bills = await checkExistThenGet(billId, Bill, { deleted: false });
            bills.deleted = true;
            await bills.save();
            let reports = {
                "action":"Delete bills",
            };
            let report = await Report.create({...reports, user: user ,belong:'SHOP'});
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};