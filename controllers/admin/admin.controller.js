import ApiError from "../../helpers/ApiError";
import Contact from "../../models/contact/contact.model";
import Shop from "../../models/shop/shop.model";
import Package from "../../models/package/package.model";
import Bills from "../../models/bills/bills.model";
import User from "../../models/user/user.model";
import Report from "../../models/reports/report.model";
const populateQuery = [ 
    { path: 'client', model: 'user' }
];
export default {
    async getLastUser(req, res, next) {
        try {
            let user = req.user;
            let {type} = req.query;
            let query = { deleted: false };
            if(type) query.type = type;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
         
            let lastUser = await User.find(query)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async getLastActions(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, 'bad auth'));
            let query = {deleted: false};
         
            let lastUser = await Report.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(10);

            res.send(lastUser);
        } catch (error) {
            next(error);
        }
    },
    async count(req,res, next) {
        try {
            let query = { deleted: false };
            const usersCount = await User.count(query);
            const shopCount = await Shop.count(query);
            const packageCount = await Package.count(query);
            const billsCount = await Bills.count(query);
            const messageCount = await Contact.count(query);
            res.status(200).send({
                users:usersCount,
                shopCount:shopCount,
                packageCount:packageCount,
                billsCount:billsCount,
                messageCount:messageCount
                
            });
        } catch (err) {
            next(err);
        }
        
    },
    
}