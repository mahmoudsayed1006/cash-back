import express from 'express';
import BillController from '../../controllers/bills/bills.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        BillController.validateBody(),
        BillController.create
    )
    .get(BillController.findAll);
    
router.route('/:billId')
    .get(BillController.findById)
    .delete( requireAuth,BillController.delete);

router.route('/:billId/accept')
    .put(
        requireAuth,
        BillController.accept
    );

router.route('/:billId/refuse')
    .put(
        requireAuth,
        BillController.refused
    );


export default router;