import express from 'express';
import userRoute from './user/user.route';
import ShopRoute  from './shop/shop.route';
import BillRoute  from './bills/bills.route';
import PackageRoute  from './package/package.route';
import ContactRoute  from './contact/contact.route';
import ReportRoute  from './reports/report.route';
import NotifRoute  from './notif/notif.route';
import AdminRoute  from './admin/admin.route';

import { requireAuth } from '../services/passport';

const router = express.Router();

router.use('/', userRoute);
router.use('/shops',ShopRoute);
router.use('/package',PackageRoute);
router.use('/bills',BillRoute);
router.use('/contact-us',ContactRoute);
router.use('/reports',requireAuth, ReportRoute);
router.use('/notif',requireAuth, NotifRoute);
router.use('/admin',requireAuth, AdminRoute);
export default router;
