import express from 'express';
import ShopController from '../../controllers/shop/shop.controller';
import BranchController from '../../controllers/branch/branch.controller';

import { requireAuth } from '../../services/passport';
import { multerSaveTo } from '../../services/multer-service';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        multerSaveTo('shops').single('img'),
        ShopController.validateBody(),
        ShopController.create
    )
    .get(ShopController.findAll);
    
router.route('/:shopId')
    .put(
        requireAuth,
        multerSaveTo('shops').single('img'),
        ShopController.validateBody(true),
        ShopController.update
    )
    .get(ShopController.findById)
    .delete( requireAuth,ShopController.delete);

    router.route('/:shopId/branches')
    .post(
        requireAuth,
        BranchController.validateAreaBody(),
        BranchController.create
    )
    .get(BranchController.getAll);

router.route('/:shopId/branches/:branchId')
    .put(
        requireAuth,
        BranchController.validateAreaBody(true),
        BranchController.update
    )
    .get(BranchController.getById)
    .delete(requireAuth,BranchController.delete);





export default router;