
import passport from 'passport';
import passportJwt from 'passport-jwt';
import passportLocal from 'passport-local';
import config from '../config';
import moment from 'moment';
import User from '../models/user/user.model';
var FacebookTokenStrategy = require('passport-facebook-token');

const JwtStrategy = passportJwt.Strategy;
const LocalStrategy = passportLocal.Strategy;
const { ExtractJwt } = passportJwt;
const { jwtSecret } = config;


passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: jwtSecret
}, (payload, done) => {
    User.findById(payload.sub).then(user => {
      
        if (!user)
            return done(null, false);

        return done(null, user)
    }).catch(err => {
        console.log('Passport Error: ', err);
        return done(null, false);
    })
}
));

//facebook
passport.use(new FacebookTokenStrategy({
    clientID: '376077783149259',
    clientSecret: '5607aa8b2707f82256470af8791e6034'
  }, function(accessToken, refreshToken, profile, done) {
    try {
        console.log('profile', profile);
        console.log('accessToken', accessToken);
        console.log('refreshToken', refreshToken);
        
        const existingUser = User.findOne({ "email": profile.emails[0].value });
        if (existingUser) {
          return done(null, existingUser);
        }
    
        const newUser = new User({
            email: profile.emails[0].value,
            firstname : profile.name.givenName ,
            lastname: profile.name.familyName,
            img:profile.photos[0].value
        });
    
        newUser.save();
        done(null, newUser);
      } catch(error) {
        done(error, false, error.message);
      }
  }
));

passport.use(new LocalStrategy({
    usernameField: 'email'
}, (email, password, done) => {

    User.findOne({ email }).then(user => {
        if (!user)
            return done(null, false);

        // Compare Passwords 
        user.isValidPassword(password, function (err, isMatch) {
            if (err) return done(err);
            if (!isMatch) return done(null, false, { error: 'Invalid password' });

            return done(null, user);
        })

    });
}));
const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignIn = passport.authenticate('local', { session: false });
const facebookSignIn = passport.authenticate('facebook', { session: false });

export {requireAuth,requireSignIn,facebookSignIn};